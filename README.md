# Playlist download
_Download the entire video with the url of the playlist or the YouTube video to be able to download it, if it is for adults, it will put a file with the title of the one that failed you and within the url_

###Prerequisites 📋

_google-api-python-client: This library provides a simple interface to interact with various Google APIs, such as the YouTube Data API. Allows you to make HTTP requests to Google services and process the responses._

_pytube: Pytube is a library for downloading YouTube videos. It makes it easy to extract information about videos, such as titles, duration and available formats, and also allows you to download the videos._

_oauth2client: This library is used for authentication with Google APIs. Helps manage OAuth 2.0 credentials needed to access resources protected by Google._


```
pip install google-api-python-client pytube oauth2client
```

###To obtain a Google API key to use with the YouTube API, follow these steps:

Access the Google Developer Console:

Go to the Google Developer Console.
If you don't have a Google account, you'll need to create one. If you already have one, log in.
Create a new project:

Click the "Select a Project" button at the top of the page, then click "New Project."
Give your project a name and click "Create."
Enable YouTube API:

In the left navigation pane, click "Library."
Search for “YouTube Data API v3” and select the result.
Click the "Enable" button on the API details page.
Set API key:

Click the "Create Credentials" button.
Select "API Key" from the drop-down menu.
Choose "Server Key."
Copy the generated API key. You can restrict the key for added security, such as limiting which apps can use it.
